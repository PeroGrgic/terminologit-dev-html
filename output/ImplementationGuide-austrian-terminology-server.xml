<?xml version="1.0" encoding="UTF-8"?>

<ImplementationGuide xmlns="http://hl7.org/fhir">
  <id value="austrian-terminology-server"/>
  <text>
    <status value="extensions"/><div xmlns="http://www.w3.org/1999/xhtml"><h2>termgit</h2><p>The official URL for this implementation guide is: </p><pre>https://termgit.elga.gv.at/ImplementationGuide/austrian-terminology-server</pre><div><p>Der Terminologie-Browser von TerminoloGit, powerd by FHIR IG Publisher.</p>
</div></div>
  </text>
  <url value="https://termgit.elga.gv.at/ImplementationGuide/austrian-terminology-server"/>
  <version value="2.4.3+20230726"/>
  <name value="termgit"/>
  <title value="Österreichischer e-Health-Terminologie-Browser"/>
  <status value="active"/>
  <date value="2023-07-26T00:35:00+00:00"/>
  <contact>
    <name value="ELGA GmbH"/>
    <telecom>
      <system value="email"/>
      <value value="mailto:cda@elga.gv.at"/>
    </telecom>
  </contact>
  <description value="Der Terminologie-Browser von TerminoloGit, powerd by FHIR IG Publisher."/>
  <packageId value="austrian-terminology-server"/>
  <license value="CC0-1.0"/>
  <fhirVersion value="4.3.0"/>
  <dependsOn id="hl7tx">
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/implementationguide-dependency-comment">
      <valueMarkdown value="Automatically added as a dependency - all IGs depend on HL7 Terminology"/>
    </extension>
    <uri value="http://terminology.hl7.org/ImplementationGuide/hl7.terminology"/>
    <packageId value="hl7.terminology.r4"/>
    <version value="5.0.0"/>
  </dependsOn>
  <dependsOn id="hl7ext">
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/implementationguide-dependency-comment">
      <valueMarkdown value="Automatically added as a dependency - all IGs depend on the HL7 Extension Pack"/>
    </extension>
    <uri value="http://hl7.org/fhir/extensions/ImplementationGuide/hl7.fhir.uv.extensions"/>
    <packageId value="hl7.fhir.uv.extensions.r4"/>
    <version value="1.0.0"/>
  </dependsOn>
  <definition>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="copyrightyear"/>
      </extension>
      <extension url="value">
        <valueString value="2020+"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="releaselabel"/>
      </extension>
      <extension url="value">
        <valueString value="TerminoloGit"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="show-inherited-invariants"/>
      </extension>
      <extension url="value">
        <valueString value="false"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="apply-version"/>
      </extension>
      <extension url="value">
        <valueString value="false"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="autoload-resources"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="path-liquid"/>
      </extension>
      <extension url="value">
        <valueString value="template/liquid"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="path-liquid"/>
      </extension>
      <extension url="value">
        <valueString value="input/liquid"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="path-qa"/>
      </extension>
      <extension url="value">
        <valueString value="temp/qa"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="path-temp"/>
      </extension>
      <extension url="value">
        <valueString value="temp/pages"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="path-output"/>
      </extension>
      <extension url="value">
        <valueString value="output"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="path-suppressed-warnings"/>
      </extension>
      <extension url="value">
        <valueString value="input/ignoreWarnings.txt"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="path-history"/>
      </extension>
      <extension url="value">
        <valueString value="https://termgit.elga.gv.at/history.html"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="template-html"/>
      </extension>
      <extension url="value">
        <valueString value="template-page.html"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="template-md"/>
      </extension>
      <extension url="value">
        <valueString value="template-page-md.html"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="apply-contact"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="apply-context"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="apply-copyright"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="apply-jurisdiction"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="apply-license"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="apply-publisher"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="active-tables"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="fmm-definition"/>
      </extension>
      <extension url="value">
        <valueString value="http://hl7.org/fhir/versions.html#maturity"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="propagate-status"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="excludelogbinaryformat"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueString value="tabbed-snapshots"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-internal-dependency">
      <valueCode value="hl7.fhir.uv.tools#current"/>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="copyrightyear"/>
      </extension>
      <extension url="value">
        <valueString value="2020+"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="releaselabel"/>
      </extension>
      <extension url="value">
        <valueString value="TerminoloGit"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="show-inherited-invariants"/>
      </extension>
      <extension url="value">
        <valueString value="false"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="apply-version"/>
      </extension>
      <extension url="value">
        <valueString value="false"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="autoload-resources"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="path-liquid"/>
      </extension>
      <extension url="value">
        <valueString value="template/liquid"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="path-liquid"/>
      </extension>
      <extension url="value">
        <valueString value="input/liquid"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="path-qa"/>
      </extension>
      <extension url="value">
        <valueString value="temp/qa"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="path-temp"/>
      </extension>
      <extension url="value">
        <valueString value="temp/pages"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="path-output"/>
      </extension>
      <extension url="value">
        <valueString value="output"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="path-suppressed-warnings"/>
      </extension>
      <extension url="value">
        <valueString value="input/ignoreWarnings.txt"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="path-history"/>
      </extension>
      <extension url="value">
        <valueString value="https://termgit.elga.gv.at/history.html"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="template-html"/>
      </extension>
      <extension url="value">
        <valueString value="template-page.html"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="template-md"/>
      </extension>
      <extension url="value">
        <valueString value="template-page-md.html"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="apply-contact"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="apply-context"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="apply-copyright"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="apply-jurisdiction"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="apply-license"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="apply-publisher"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="active-tables"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="fmm-definition"/>
      </extension>
      <extension url="value">
        <valueString value="http://hl7.org/fhir/versions.html#maturity"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="propagate-status"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="excludelogbinaryformat"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <extension url="http://hl7.org/fhir/tools/StructureDefinition/ig-parameter">
      <extension url="code">
        <valueCode value="tabbed-snapshots"/>
      </extension>
      <extension url="value">
        <valueString value="true"/>
      </extension>
    </extension>
    <resource>
      <extension url="http://hl7.org/fhir/tools/StructureDefinition/resource-information">
        <valueString value="CodeSystem"/>
      </extension>
      <reference>
        <reference value="CodeSystem/appc-anatomie"/>
      </reference>
      <name value="APPC_Anatomie"/>
      <description value="**Description:**  Code List / CodeSystem for all APPC-Codes for 4th Axis: Anatomy&#xA;&#xA;**Beschreibung:** Code Liste aller Codes der 4. APPC-Achse: Anatomie&#xA;&#xA;**Versions-Beschreibung:** Korrekturversion zu 1.0.10"/>
      <exampleBoolean value="false"/>
    </resource>
    <resource>
      <extension url="http://hl7.org/fhir/tools/StructureDefinition/resource-information">
        <valueString value="ConceptMap"/>
      </extension>
      <reference>
        <reference value="ConceptMap/elga-lebensmittelallergene-mapping"/>
      </reference>
      <name value="ELGA Lebensmittelallergene Mapping"/>
      <description value="Diese ConceptMap stellt ein Mapping von den Allergien aus SNOMED CT auf die Lebensmittelallergene gemäß EU Verordnung (https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:02011R1169-20180101&amp;from=DE#E0017) dar."/>
      <exampleBoolean value="false"/>
    </resource>
    <resource>
      <extension url="http://hl7.org/fhir/tools/StructureDefinition/resource-information">
        <valueString value="ValueSet"/>
      </extension>
      <reference>
        <reference value="ValueSet/appc-anatomie"/>
      </reference>
      <name value="APPC_Anatomie"/>
      <description value="**Description:** Value Set for all APPC-Codes for 4th Axis 'Anatomy'&#xA;&#xA;**Beschreibung:** Value Set aller Codes der 4. APPC-Achse 'Anatomie'"/>
      <exampleBoolean value="false"/>
    </resource>
    <page>
      <nameUrl value="toc.html"/>
      <title value="Table of Contents"/>
      <generation value="html"/>
      <page>
        <nameUrl value="index.html"/>
        <title value="Home"/>
        <generation value="markdown"/>
      </page>
      <page>
        <nameUrl value="index_en.html"/>
        <title value="Home (en)"/>
        <generation value="markdown"/>
      </page>
      <page>
        <nameUrl value="arch_and_setup_de.html"/>
        <title value="Architektur und Installation (de)"/>
        <generation value="markdown"/>
      </page>
      <page>
        <nameUrl value="arch_and_setup_en.html"/>
        <title value="Architecture and Setup (en)"/>
        <generation value="markdown"/>
      </page>
    </page>
    <parameter>
      <code value="path-resource"/>
      <value value="input/capabilities"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/examples"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/extensions"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/models"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/operations"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/profiles"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/resources"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/vocabulary"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/maps"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/testing"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="input/history"/>
    </parameter>
    <parameter>
      <code value="path-resource"/>
      <value value="fsh-generated/resources"/>
    </parameter>
    <parameter>
      <code value="path-pages"/>
      <value value="template/config"/>
    </parameter>
    <parameter>
      <code value="path-pages"/>
      <value value="input/images"/>
    </parameter>
    <parameter>
      <code value="path-tx-cache"/>
      <value value="input-cache/txcache"/>
    </parameter>
  </definition>
</ImplementationGuide>